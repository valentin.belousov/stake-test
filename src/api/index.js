const apiKey = "RRRCTYRXWIT9RMHRKRK3IP6RBRFZZPQ113";

export const getAbi = async (token) => {
  return await fetch(
    `https://api.etherscan.io/api?module=contract&action=getabi&address=${token}&apiKey=${apiKey}`
  )
    .then((r) => r.json())
    .then((json) => json.result);
};

export const getTokenPrice = (ids = "", vsCurrency = "usd") =>
  fetch(
    `https://api.coingecko.com/api/v3/simple/price?ids=${ids}&vs_currencies=${vsCurrency}`
  )
    .then((res) => res.json())
    .then((res) => res[ids][vsCurrency]);
