export const roundEth = (balance, mod = 1e4) =>
  String(Math.round(balance * mod) / mod);
